
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FireBallSpawner : MonoBehaviour
{

    public GameObject fireball;
    private float minRandom, maxRandom;
    private float nextActionTime = 0;
    public float period = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        minRandom = transform.position.x - 10f;
        maxRandom = transform.position.x + 10f;
    }


    void Update()
    {
        nextActionTime += Time.deltaTime;
        if (nextActionTime > period)
        {
            nextActionTime = 0.0f;
            Spawn();
        }
    }
    void Spawn()
    {

        Instantiate(fireball, new Vector3(Random.Range(minRandom, maxRandom), transform.position.y, 0), Quaternion.identity);
    }
}
