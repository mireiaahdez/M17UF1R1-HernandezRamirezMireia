using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FireBallSpawnerMode2 : MonoBehaviour
{
    

    [SerializeField]
    private GameObject fireball;

    
   

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Instantiate(fireball, new Vector3(cursorPos.x, cursorPos.y, 0), Quaternion.identity);
        }
    }
}
