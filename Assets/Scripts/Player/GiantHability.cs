using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class GiantHability : MonoBehaviour
{
    public enum StatusPlayer
    {
        
        Grow,
        Use,
        Ungrow,
        Cooldown,
        Avaliable
        
    }
    public StatusPlayer currentState;
    private float playerHeight;
    private Transform growingUp;
    private int cooldown;
    public int waiting;
    public int duration;
    public DataPlayer _dataPlayer;
    


    void Start()
    {
        _dataPlayer = gameObject.GetComponent<DataPlayer>();
        growingUp = gameObject.GetComponent<Transform>();
        currentState = StatusPlayer.Avaliable;
        playerHeight = _dataPlayer.height;
    }


    void Update()
    {

        if (currentState == StatusPlayer.Avaliable) playerHeight = _dataPlayer.height;
        else if (currentState == StatusPlayer.Grow)
        {
            HabilityUse(0.5f);
            if (growingUp.localScale.x >= 10)
            {
                cooldown = duration * 10;
                currentState = StatusPlayer.Use;
            }
        }
        else if (currentState == StatusPlayer.Use)
        {
            cooldown--;
            if (cooldown == 0) currentState = StatusPlayer.Ungrow;

        }else if(currentState == StatusPlayer.Ungrow)
        {
            HabilityUse(-0.5f);
            if (growingUp.localScale.x <= 5)
            {
                cooldown = duration * 10;
                currentState = StatusPlayer.Cooldown;
            }
        }else if(currentState == StatusPlayer.Cooldown)
        {
            cooldown--;
            if(cooldown == 0) currentState = StatusPlayer.Avaliable;
        }


    }

    public void HabilityUse(float increase)
    {
        _dataPlayer.height += (playerHeight/4) * increase;
        growingUp.localScale = new Vector3(growingUp.localScale.x + increase, growingUp.localScale.y + increase, growingUp.localScale.z);
    }
}
