using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PlayerRoles
{
    Knight,
    Warrior,
    Sorcerer,
    Mage
}

public class DataPlayer : MonoBehaviour
{



    public PlayerRoles _playerRoles;

    [SerializeField]
    private float forceJump;
    public float height;
    public float speed;
    public bool isWalking;
    public bool isJumping;
    public int dead =0;
    private float movement;
    private bool _isFlipping = true;
    private Animator _anim;
    private Rigidbody2D _rb;

    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
    }

    private void Update()
    {
        movement = Input.GetAxisRaw("Horizontal");
        Movement();
        Jumping();
        Flip();
        if(transform.position.y < -6f)
        {
            dead = 1;
            SceneManager.LoadScene(2);
        }
        PlayerPrefs.SetInt("Dead", dead);
    }

    private void Flip()
    {
        if (_isFlipping && movement < 0f || !_isFlipping && movement > 0f)
        {
            _isFlipping = !_isFlipping;
            Vector3 scale = transform.localScale;
            scale.x *= -1f;
            transform.localScale = scale;
        }
    }

    private void Jumping()
    {
        if(Input.GetKeyDown(KeyCode.Space) && !isJumping)
        {
            _rb.AddForce(new Vector2(_rb.velocity.x, forceJump));
            _anim.SetBool("isJumping", false);
        }
    }

    private void Movement()
    {
        _rb.velocity = new Vector2(movement*speed, _rb.velocity.y);
        if (Mathf.Abs(_rb.velocity.x) < 0.1f)
        {
            _anim.SetBool("IsWalking", false);
            _anim.SetBool("IsIdle", true);
        }
        else
        {
            _anim.SetBool("IsWalking", true);
            _anim.SetBool("IsIdleling", false);
        }

    }
}

