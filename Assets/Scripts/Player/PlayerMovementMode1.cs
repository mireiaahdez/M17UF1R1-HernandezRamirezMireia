using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementMode1 : MonoBehaviour
{
    private float move;
    private float speed = 0.02f;
    private float force = 4f;
    public bool isJumping = false;


    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private Rigidbody2D rb;
    private RaycastHit2D raycastHit2D;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("Jump", isJumping);
        Move();
        if (Input.GetKey(KeyCode.Space) && !isJumping)
        {
            Jumping();
        }
    }


    private void Jumping()
    {
        rb.AddForce(Vector2.up * force, ForceMode2D.Impulse);
    }

    private void Move()
    {
        move = Input.GetAxis("Horizontal");
        animator.SetBool("Walk", move != 0.0f);
        transform.position = new Vector3(move * speed + transform.position.x, transform.position.y, 0);
        if (move > 0.0f)
        {
            spriteRenderer.flipX = false;
        }
        else if (move < 0.0f)
        {
            spriteRenderer.flipX = true;
        }


    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
            rb.velocity = new Vector2(rb.velocity.x, 0);
        }
    }

}
