using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovementMode2 : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    private float speed = 0.01f;
    private int direction = 1;

    private RaycastHit2D ground;
    private RaycastHit2D fireball;
    private bool raycast = false;

    // Start is called before the first frame update
    void Start()
    {
        
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }


        // Update is called once per frame
    void Update()
    {
        Movement();
        Physics2D.queriesStartInColliders = false;
      
        ground = Physics2D.Raycast(this.transform.position, new Vector3(direction, -1, 0));

        if (!ground) raycast = true;
        else if (ground.collider.tag == "Ground") raycast = false;
    }


    private void Movement()
    {
        if (raycast)
        {
            speed *= -1;
            spriteRenderer.flipX = (spriteRenderer.flipX) ? false : true; 
            direction *= -1;
           
        }

        transform.position += new Vector3(speed, 0, 0);
        animator.SetBool("isWalking", speed != 0.0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "fireball")
        {
            animator.SetBool("dead", true);
            SceneManager.LoadScene(4);
        }
    }


}
