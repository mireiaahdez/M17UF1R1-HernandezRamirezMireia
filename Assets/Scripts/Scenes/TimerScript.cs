using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class TimerScript : MonoBehaviour
{
    private float timeLeft = 60f;
    public bool timeOn = false;
    public int timeOn1 = 0;
    public Text TimerText;
    // Start is called before the first frame update
    void Start()
    {
        timeOn = true;
        timeOn1 = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (timeOn)
        {
            if (timeLeft > 0f)
            {
                timeLeft -= Time.deltaTime;
                updateTimer(timeLeft);
                timeOn1 = 1;
            }
            else
            {
                Debug.Log("Time is Up!");
                timeLeft = 0f;
                timeOn = false;
                timeOn1 = 2;
            }
        }
        PlayerPrefs.SetInt("Timer", timeOn1);
        timeOn1 = 0;
    }
    void updateTimer(float currentTime)
    {
        currentTime += 1f;
        float minutes = Mathf.FloorToInt(currentTime / 60);
        float secons = Mathf.FloorToInt(currentTime % 60);
        TimerText.text = string.Format("{0:00} : {1:00}", minutes, secons);
    }
}
