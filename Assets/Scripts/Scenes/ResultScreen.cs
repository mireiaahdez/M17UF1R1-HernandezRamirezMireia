using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreen : MonoBehaviour
{
    private string name;
    private int wins;
    private int scene;

    private AudioSource audio;
    public AudioClip winSound;
    public AudioClip lostSound;

    private GameObject nameText;
    private GameObject fireballsText;
    private GameObject win;
    private GameObject lost;


    // Start is called before the first frame update
    void Start()
    {
        name = PlayerPrefs.GetString("name");
        wins = PlayerPrefs.GetInt("wins");
        scene = PlayerPrefs.GetInt("scene");

        nameText = GameObject.Find("Name");
        nameText.GetComponent<Text>().text = name;

        fireballsText = GameObject.Find("FireBalls");
        

        audio = GetComponent<AudioSource>();



        if(scene != 8)
        {
            if(wins == 1)
            {
                lost = GameObject.Find("LOST");
                lost.SetActive(false);
                audio.clip = winSound;
                audio.Play();

            }
            else
            {
                win = GameObject.Find("WIN");
                win.SetActive(false);
                audio.clip = lostSound;
                audio.Play();
            }
        }
        else
        {
            if (wins != 1)
            {
                lost = GameObject.Find("LOST");
                lost.SetActive(false);
                audio.clip = winSound;
                audio.Play();

            }
            else
            {
                win = GameObject.Find("WIN");
                win.SetActive(false);
                audio.clip = lostSound;
                audio.Play();
            }
        }
    }

    
}
