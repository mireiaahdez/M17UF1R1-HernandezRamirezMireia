using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{

   
    private InputField inputField;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MenuScreen()
    {
        SceneManager.LoadScene(0);
    }

    public void GameScreen1()
    {
        SceneManager.LoadScene(1);
    }

    public void GameScreen2()
    {
        SceneManager.LoadScene(2);
    }
    public void GameOverScreen()
    {
        SceneManager.LoadScene(3);
    }
}
