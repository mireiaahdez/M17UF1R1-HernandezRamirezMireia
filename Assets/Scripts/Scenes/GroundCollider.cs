using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GroundCollider : MonoBehaviour
{

    private int wins = 0;
    private string _name;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _name = PlayerPrefs.GetString("mname");
            Destroy(collision.gameObject);
            PlayerPrefs.SetString("name", _name);
            PlayerPrefs.SetInt("win", wins);
            SceneManager.LoadScene(3);
        }
    }
}
